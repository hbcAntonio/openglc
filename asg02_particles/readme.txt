Program extra features implemented:
- Wind
- Lights

Program keyboard commands:
- 'F' toggles friction on/off
- 'W' toggles wind on/off
- 'G' toggles gravity on/off
- SPACE toggles particle spawn on/off
- 'R' resets particle system
- ESC exits the programs

* not case-sensitive

PLATFORM USED TO PROGRAM:
MacOSX

- Notable difference in update times

NO IDE USED:

- Makefile included