#include "apsystem.h"

class Asg02{
public:

	int screen_width;
	int screen_height;

	Vector3f scene_rotation;

	void update();
	void display();
	void special(int, int, int);
	void keyboard(unsigned char, int, int);

	AParticleSystem *particleSystem;

	Asg02(unsigned int);
	~Asg02();
	

};