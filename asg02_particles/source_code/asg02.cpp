#include "asg02.h"

/* Special keys for scene rotation */
void Asg02::special(int key, int x, int y){
	if (key == GLUT_KEY_UP){
		if (scene_rotation.y < 60.0f)
			scene_rotation.y++;
	} else if (key == GLUT_KEY_DOWN){
		if (scene_rotation.y > 0.0f)
			scene_rotation.y--;
	} else if (key == GLUT_KEY_RIGHT){
		scene_rotation.x--;
	} else if (key == GLUT_KEY_LEFT){
		scene_rotation.x++;
	}

	// Clamp x rotation
	if (scene_rotation.x > 360.0f) scene_rotation.x = 1.0f;
}

/* Keyboard handling */
void Asg02::keyboard(unsigned char key, int x, int y){
	if (key == 'F' || key == 'f') particleSystem->applyFriction = !particleSystem->applyFriction;
	if (key == 'W' || key == 'w') particleSystem->windOn = !particleSystem->windOn;
	if (key == 'G' || key == 'g') particleSystem->gravityOn = !particleSystem->gravityOn;
	if (key == 'R' || key == 'r') particleSystem->particles.clear();
	if (key == 32) particleSystem->animate = !particleSystem->animate;

}

/* Main application update method */
void Asg02::update(){
	particleSystem->update();

	// Particle-plane collision is handled separately
	// as particle system does not know what it may
	// collide with in the scene
	int psize = particleSystem->particles.size();
	for (int i = 0; i < psize; ++i){
		Vector3f position = particleSystem->particles[i]->position;
		Vector3f direction = particleSystem->particles[i]->direction;
		if (position.y <= 0.5f){
			if (position.x > -7.5f && position.x < 7.5f){
				if (position.z > -7.5f && position.z < 7.5f){
					// Particle y is inside plane
					particleSystem->particles[i]->direction.y = 1.0f;
					particleSystem->particles[i]->direction = particleSystem->particles[i]->direction.normalized();
				
					// Friction applies
					if (particleSystem->applyFriction){
						if (particleSystem->particles[i]->mspeed > 0.0f){
							particleSystem->particles[i]->mspeed -= particleSystem->particles[i]->friction;
						}
					}

					// Checks if movement can overcome gravity
					if (particleSystem->particles[i]->mspeed <
						particleSystem->gravity.magnitude()){
						particleSystem->particles[i]->position.y = 0.5f;
					}

					// Calculates wind influence
					if (particleSystem->windOn){
						particleSystem->particles[i]->mspeed += particleSystem->wind.magnitude() * 0.01f;
					}
				}
			}
		}
	}
}

/* Main application display method */
void Asg02::display(){

	glPushMatrix();
		// Camera rotation simulated by turning every
		// geometry in the current scene
		glRotatef(scene_rotation.y, 1.0f, 0.0f, 0.0f);
		glRotatef(scene_rotation.x, 0.0f, 1.0f, 0.0f);

		// Draw main cube
		glPushMatrix();
			float m_diffuse[] = {0.4f, 0.4f, 0.4f, 1.0f};
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, m_diffuse);
			glTranslatef(0.0f, 0.0f, 0.0f);
			glScalef(15.0f, 0.5f, 15.0f);
			glutSolidCube(1.0f);
		glPopMatrix();

		// Debug cubes that for some reason
		// look nice enough
		glPushMatrix();
			glTranslatef(-7.5f, 0.0f, 0.0f);
			glutSolidCube(1.0f);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(7.5f, 0.0f, 0.0f);
			glutSolidCube(1.0f);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.0f, 0.0f, 7.5f);
			glutSolidCube(1.0f);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.0f, 0.0f, -7.5f);
			glutSolidCube(1.0f);
		glPopMatrix();

		// Particle system draw method
		particleSystem->draw();

	glPopMatrix();
}

Asg02::Asg02(unsigned int msUpdate){
	int maxParticles = 1000;
	int particlesPerSecond = 10;

	// Initiate particle system
	particleSystem = new AParticleSystem(
		Vector3f(0.0f, 4.0f, 0.0f), 				// Origin
		Vector3f(0.0f, 1.0f, 0.5f).normalized(), 	// Direction
		particlesPerSecond, 						// Particles per second
		maxParticles,								// Max particles
		msUpdate);									// Milisseconds per update call

	// Default settings
	particleSystem->setGravity(Vector3f(0.0f, -0.1f, 0.0f));
	particleSystem->setWind(Vector3f(0.05f, 0.0f, -0.1f));
	particleSystem->gravityOn = true;
	particleSystem->applyFriction = true;
	particleSystem->animate = true;
}

Asg02::~Asg02(){}