#include "apsystem.h"
#include <cstdlib>
#include <ctime>

/* Set general particle system gravity */
void AParticleSystem::setGravity(Vector3f gravity){
	this->gravity = gravity;
}

/* Set particle system wind force */
void AParticleSystem::setWind(Vector3f wind){
	this->wind = wind;
}

/* Spawn particles accordingly to particlesPerSecond
and maxParticles settings from the system */
void AParticleSystem::spawnParticles(){
	if (animate){
		if (particles.size() > maxParticles) return;

		for (int i = 0; i < particlesPerSecond; ++i){

			// Particle material colour
			Vector3f m_ambient((float) rand() / (float) RAND_MAX, 
							   (float) rand() / (float) RAND_MAX,
							   (float) rand() / (float) RAND_MAX);

			// Particle initial orientation
			Vector3f displacement(rand()%10+1, rand()%10+1, rand()%10+1);
			Vector3f orientation = (this->orientation + displacement).normalized();
			
			// Particle initial rotation
			Vector3f rot_direction(rand()%10+1, rand()%10+1, rand()%+1);
			rot_direction = rot_direction.normalized();
			float rspeed = (((float) rand() / (float) RAND_MAX) * 12.0f) + 1.0f;

			// Particle initial speed and size calculation
			float speed = 0.2f;
			float size = (float)rand()/(float)(RAND_MAX * 5.0f) + 0.1f;

			// Particle shape
			int shape = rand()%2;

			AParticle *particle = new AParticle(
				this->origin, 					// Initial position
				orientation, 				    // Direction to go
				Vector3f(0.0f, 0.0f, 0.0f),		// Rotation
				rot_direction,				    // Rotation orientation
				m_ambient,						// Material
				speed,							// Speed
				rspeed,							// Rotation speed
				size, 							// Size
				2000, 							// Duration in milisseconds
				speed * 0.1f,					// Friction
				shape							// Shape
			);

			particles.push_back(particle);
		}
	}
}

/* Update method */
void AParticleSystem::update(){

	int psize = particles.size();
	for (int i = 0; i < psize; ++i){

		// Calculate particle position updates
		particles[i]->updatePosition();
		if (gravityOn && particles[i]->mspeed > 0.0f){
			particles[i]->direction = (particles[i]->direction + gravity).normalized();
			particles[i]->applyForce(gravity);
		}

		// Apply wind
		if (windOn) particles[i]->applyForce(wind);

		// Calculate particle life update
		particles[i]->lifespan += msPerUpdate;
		if (particles[i]->lifespan > particles[i]->duration){
			particles.erase(particles.begin() + i);
			psize--;
			i--;
		}
	}

	
}

/* Draw method */
void AParticleSystem::draw(){
	int psize = particles.size();
	for (int i = 0; i < psize; ++i){
		particles[i]->drawParticle();
	}
}

/* Constructor and destructor methods */
AParticleSystem::AParticleSystem(Vector3f origin, Vector3f orientation, int particlesPerSecond, int maxParticles, unsigned int msPerUpdate){
	this->origin = origin;
	this->orientation = orientation;
	this->particlesPerSecond = particlesPerSecond;
	this->maxParticles = maxParticles;
	this->msPerParticle = 1000.0f / particlesPerSecond;
	this->msPerUpdate = msPerUpdate;

	// Control variables
	this->animate = true;
	this->gravityOn = false;
	this->applyFriction = false;
	this->windOn = false;

	// Random number
	srand (time(NULL));

}

AParticleSystem::~AParticleSystem(){}