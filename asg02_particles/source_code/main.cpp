#include <iostream>
#include <stdio.h>
#include "asg02.h"
#define MS_UPDATE 33
using namespace::std;

Asg02 *application;

void spawnParticles(int value){
	application->particleSystem->spawnParticles();
	glutTimerFunc(application->particleSystem->msPerParticle, spawnParticles, 0);
}

void update(int value){
	application->update();
	glutTimerFunc(MS_UPDATE, update, value);
}

void special(int key, int x, int y){
	application->special(key, x, y);
}

void keyboard(unsigned char key, int x, int y){
	application->keyboard(key, x, y);

	if (key == 27) exit(0);
}

void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	application->display();

	glutSwapBuffers();
	glutPostRedisplay();
}

void init(){

	// Instantiate application
	application = new Asg02(MS_UPDATE);

	// OpenGL standard configurations
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, 1.3f, 1, 100);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Light configurations
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	float l_position[] = {3.5f, 10.0f, 7.5f, 0};
	float l_ambient[] = {0.1f, 0.1f, 0.1f, 1.0f};
	float l_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	float l_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};

	glLightfv(GL_LIGHT0, GL_POSITION, l_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, l_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l_specular);

	// General material configurations
	float m_ambient[] = {0.2f, 0.2f, 0.2f, 1.0f};
	float m_specular[] = {0.8f, 0.7f, 0.7f, 1.0f};
	float shiny_factor = 10.0f;

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, m_ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, m_specular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shiny_factor);

}

int main(int argc, char** argv){

	// Initialisation functions
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Antonio's Particle System");

	// Init function
	init();

	// Update application
	glutTimerFunc(MS_UPDATE, update, 1);
	glutTimerFunc(application->particleSystem->msPerParticle, spawnParticles, 0);

	// Callback functions setup
	glutDisplayFunc(display);
	glutSpecialFunc(special);
	glutKeyboardFunc(keyboard);

	// Make camera look to the origin
	gluLookAt (0.0f, 3.0f, 15.0f,
			   0.0f, 0.0f, 0.0f,
			   0.0f, 1.0f, 0.0f);

	// Starts main loop
	glutMainLoop();


}