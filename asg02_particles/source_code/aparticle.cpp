#include "aparticle.h"
#include <stdio.h>

/* Check if particle is stationary
or not, in order to perform position updates */
bool AParticle::checkSpeed(){
	if (mspeed < 0.0f) mspeed = 0.0f;
	if (mspeed == 0.0f) return false;
	else return true;
}

/* Calculate new position vector
according to movement orientation
and movement speed */
void AParticle::updatePosition(){
	// Particle is stationary if speed is less than 0
	if (!checkSpeed()) return;

	position = position + (direction * mspeed);
	rotation = rotation + (rot_direction * rspeed);
}

/* Apply general force to particle's position vector */
void AParticle::applyForce(Vector3f force){
	position = position + force;
}

/* Draw particle method */
void AParticle::drawParticle(){
	
	glPushMatrix();
		float diffuse[] = {m_diffuse.x, m_diffuse.y, m_diffuse.z, 1.0f};
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);

		glTranslatef(position.x, position.y, position.z);

		glRotatef(rotation.x, 1.0f, 0.0f, 0.0f);
		glRotatef(rotation.y, 0.0f, 1.0f, 0.0f);
		glRotatef(rotation.z, 0.0f, 0.0f, 1.0f);

		if (shape == CUBE) glutSolidCube(size);
		if (shape == SPHERE) glutSolidSphere(size, 20, 20);
	glPopMatrix();
}

/* Constructor and destructor methods */
AParticle::AParticle(Vector3f position, Vector3f direction, Vector3f rotation,
Vector3f rot_direction, Vector3f m_diffuse, float mspeed, float rspeed,
float size, float duration, float friction, int shape){

	this->position = position;
	this->direction = direction;
	this->rotation = rotation;
	this->rot_direction = rot_direction;
	this->m_diffuse = m_diffuse;
	this->mspeed = mspeed;
	this->rspeed = rspeed;
	this->size = size;
	this->duration = duration;
	this->lifespan = 0.0f;
	this->friction = friction;
	this->shape = (ParticleShapes) shape;
}

AParticle::~AParticle(){}