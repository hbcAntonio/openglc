#include "aparticle.h"
#include <vector>
using namespace::std;

class AParticleSystem{
public:
	Vector3f origin;
	Vector3f orientation;
	Vector3f gravity;
	Vector3f wind;

	int particlesPerSecond;
	int maxParticles;

	float msPerParticle;
	float msPerUpdate;

	bool gravityOn;
	bool windOn;
	bool applyFriction;
	bool animate;

	vector<AParticle*> particles;

	void update();
	void draw();
	void spawnParticles();
	void setGravity(Vector3f);
	void setWind(Vector3f);

	AParticleSystem(Vector3f, Vector3f, int, int, unsigned int);
	~AParticleSystem();
};