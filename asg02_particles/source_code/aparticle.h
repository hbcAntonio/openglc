http://stackoverflow.com/questions/5915753/generate-a-plane-with-triangle-strips

#include "vector3f.h"
#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#  include <GL/freeglut.h>
#endif

class AParticle{
public:
	enum ParticleShapes{CUBE=0, SPHERE};
	ParticleShapes shape;

	Vector3f position;
	Vector3f direction;
	Vector3f rotation;
	Vector3f rot_direction;
	Vector3f m_diffuse;

	float mspeed;
	float rspeed;
	float size;
	float lifespan;
	float duration;
	float friction;

	void updatePosition();
	void applyForce(Vector3f);
	void drawParticle();

	bool checkSpeed();

	AParticle(Vector3f, Vector3f, Vector3f, Vector3f, Vector3f, float, float, float, float, float, int);
	~AParticle();
};