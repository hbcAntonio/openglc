#include <stdio.h>
#include <stdlib.h>
#include "asg03.h"
#define MS_UPDATE 33
using namespace::std;

Asg03 *application;

/* Update calculations. Not currently 
needed by Assignment 03*/
void update(int value){
	application->update();
	glutTimerFunc(MS_UPDATE, update, value);
}

/* Handles special keys */
void special(int key, int x, int y){
	application->special(key, x, y);

}

/* Keyboard handler function */
void keyboard(unsigned char key, int x, int y){
	application->keyboard(key, x, y);

	if (key == 27) exit(0);
}

/* Display function running on double buffer */
void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	application->display();

	glutSwapBuffers();
	glutPostRedisplay();
}

/* Init function, intialises standard OpenGL
components, enables light, calculates
default material properties and
viewport configuration */
void init(){

	// Instantiate application
	application = new Asg03(MS_UPDATE);

	// OpenGL standard configurations
	glClearColor(0.2f, 0.2f, 0.7f, 1.0f);
	glColor3f(1.0f, 1.0f, 1.0f);

	// Enables
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// Projection mode
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, 1.3f, 1, 200);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Which face should be culled
	glCullFace(GL_BACK);

	// Light configurations
	float l_position[] = {application->terrain->width * 0.5f, 1.0f, 
						  application->terrain->length * 0.5f, 0};
	float l_ambient[] = {0.1f, 0.1f, 0.1f, 1.0f};
	float l_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	float l_specular[] = {0.5f, 0.5f, 0.5f, 1.0f};

	glLightfv(GL_LIGHT0, GL_POSITION, l_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, l_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l_specular);

	// General material configurations
	float m_ambient[] = {0.2f, 0.2f, 0.2f, 1.0f};
	float m_specular[] = {0.4f, 0.4f, 0.4f, 1.0f};
	float shiny_factor = 30.0f;

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, m_ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, m_specular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shiny_factor);

}

int main(int argc, char** argv){

	// Initialisation functions
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Assignment 03 - Terrain");

	// Init function
	init();

	// Update application - Not needed for Assignment 03
	// glutTimerFunc(MS_UPDATE, update, 1);

	// Callback functions setup
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);

	// Make camera look to the origin
	gluLookAt (20.0f, 20.0f, 60.0f,
			   20.0f, 0.0f, 0.0f,
			   0.0f, 1.0f, 0.0f);

	// Starts main loop
	glutMainLoop();


}