#include "vector3f.h"
class AdvancedCamera{
	public:
		// Main camera orientation vectors
		Vector3f up;
		Vector3f right;
		Vector3f forward;
		Vector3f position;
		Vector3f rotation;
		Vector3f rotspeed;
		Vector3f lookat;
		float camSpeed;
		float maxYAngle;

		// Constructor and destructor methods
		AdvancedCamera(Vector3f);
		~AdvancedCamera();

		// Movement and rotation methods
		void rotateY(float angle);
		void rotateX(float angle);
		void move(float angle);
		void strafe(float angle);
		void updateLook();
};