#include "terrain.h"
#include "advcam.h"
#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#  include <GL/freeglut.h>
#endif

class Asg03{	

public:
	void keyboard(unsigned char, int, int);
	void special(int, int, int);
	void update();
	void display();
	void fetchData();
	void defineVertexColour(int, int, bool);
	void drawGridOnScene();

	// Terrain and camera instances
	Terrain *terrain;
	AdvancedCamera *camera;

	// Control attributes
	float msupdate;
	float twidth;
	float tlenght;

	enum DrawMode{FLAT = 0, SMOOTH, WIREFRAME};
	DrawMode appDrawMode;

	Asg03(float);
	~Asg03();
};