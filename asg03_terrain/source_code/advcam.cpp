#include "advcam.h"
#include <stdio.h>
#define PI 3.1415

/* Constructor initialises default values for 
all camera attributes */
AdvancedCamera::AdvancedCamera(Vector3f origin){
	position = origin;
	forward = Vector3f(0.0f, 0.0f, -1.0f);
	right = Vector3f(1.0f, 0.0f, 0.0f);
	up = Vector3f(0.0f, 1.0f, 0.0f);
	rotation = Vector3f(0.0f, 0.0f, 0.0f);
	rotspeed = Vector3f(4.0f, 4.0f, 0.0f);
	lookat = position + forward;
	camSpeed = 4.0f;
	maxYAngle = 60.0f;
}

AdvancedCamera::~AdvancedCamera(){}

/* Method to rotate around the Y axis,
effectively moving the horizontal lookat */
void AdvancedCamera::rotateY(float angle){

	// Clamp rotation
	rotation.y += angle;
	if (rotation.y > 360.0f) rotation.y -= 360.0f;
	if (rotation.y < 0.0f) rotation.y = -rotation.y;

	// Moving in the unitary circle
	float cosine = cos ( angle * (PI/180.0f));
	float sine = sin( angle * ( PI/180.0f));

	// Forward is the cross product of the
	// applied sine and cosine calculations
	forward = (forward * cosine) - (right * sine);
	forward = forward.normalized();

	// Find new right vector
	right = forward * up;

	// Update lookat vector
	updateLook();
}

/* Method to rotate around the X axis,
effectively moving the vertical lookat */
void AdvancedCamera::rotateX(float angle){
	
	// Clamp rotation and limit range
	rotation.x += angle;
	if (rotation.x > maxYAngle){
		rotation.x = maxYAngle;
		return;
	} else if (rotation.x < -maxYAngle){
		rotation.x = -maxYAngle;
		return;
	}

	// Unitary circle calculation
	float cosine = cos ( angle * (PI/180.0f));
	float sine = sin( angle * ( PI/180.0f));

	// Forward is the cross product of the
	// applied sine and cosine calculations
	forward = (forward * cosine) - (up * sine);
	forward = forward.normalized();

	// Update lookat vector
	updateLook();
}

/* Move camera forward or backwards */
void AdvancedCamera::move(float speed){
	position = position + (forward * speed);
	updateLook();
}

/* Strafe camera to the left or to the right */
void AdvancedCamera::strafe(float speed){
	position = position + (right * speed);
	updateLook();
}

/* Update lookat vector */
void AdvancedCamera::updateLook(){
	lookat = position + forward;
}