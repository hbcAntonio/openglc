#include "vector3f.h"
class Terrain{

public:
	// Terrain width and terrain length
	unsigned int width;
	unsigned int length;

	// Heightmap and normal matrices
	float **heightmap;
	Vector3f **map_normals;

	// Control attributes
	float lowest, heighest;
	bool drawHeightColour;

	// Terrain generation algorithm
	enum GenerationAlgorithms{FAULT=0, CIRCLES};
	GenerationAlgorithms genAlgorithm;

	// Terrain methods
	void generateTerrainPlane();
	void calculateNormals();
	void findHeightestAndLowest();
	float convertToRange(float);
	Vector3f getVertexColour(int, int);

	// Constructor and destructor
	Terrain(unsigned int, unsigned int);
	~Terrain();
};