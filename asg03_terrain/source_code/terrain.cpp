#include "terrain.h"
#include <cmath>
#include <ctime>
#include <stdlib.h>
#include <iostream>
using namespace::std;

/* Terrain class constructor takes two arguments */
Terrain::Terrain(unsigned int width, unsigned int length){
	
	// As width and height are provided as quads
	// and we draw vertices, add 1 to each dimension
	this->width = width +1;
	this->length = length +1;
	
	// By default, draw terrain in grayscale
	// and with the fault algorithm
	drawHeightColour = false;
	genAlgorithm = FAULT;

	// Initialise random seed
	srand(time(NULL));

	// Generate the terrain
	generateTerrainPlane();
}

/* Terrain destructor class */
Terrain::~Terrain(){
	// Delete matrices
	delete[] heightmap;
	delete[] map_normals;
}

/* Method use to effectively generate the
terrain, based on the algorithm active */
void Terrain::generateTerrainPlane(){

	// Initialise the heightmap
	heightmap = new float*[length];
	for (int i = 0; i < length; ++i)
		heightmap[i] = new float[width];

	// Address 0 value to every height
	// in the terrain by default
	for (int z = 0; z < length; ++z){
		for (int x = 0; x < width; ++x){
			heightmap[z][x] = 0.0f;
		}
	}

	// Declare displacement factor
	float displacement;

	// Fault algorithm
	if (genAlgorithm == FAULT){

		// General displacement is constant
		displacement = 0.5f;

		// Have the algorithm applied 200 times
		// (200 iterations)
		for (int i = 0; i < 200; ++i){

			// Algorithm variables
			float v = rand();

			// Normal equation constants
			float a = sin(v);
			float b = cos(v);

			// Parameters for distance calculation
			float d = sqrt(width*width + length*length);
			float c = ((float)rand() / (float)RAND_MAX) * d - d/2;

			// For every element inside the heightmap matrix
			for (int x = 0; x < width; x++){
				for (int z = 0; z < length; z++){

					// Calculate effective distance from point
					// to the random calculated line
					float effdist = (a * x) + (b * z) - c;

					// Applies displacement based on
					// effective distance calculation
					if (effdist > 0) heightmap[z][x] += displacement;
					else heightmap[z][x] -= displacement;
				}
			}
		}
	} 
	// Circles algorithm
	else if (genAlgorithm == CIRCLES){

		// Displacement is constant for circles as well
		displacement = 2.0f;

		// Iterations: 200
		for (int i = 0; i < 200; ++i){

			// Find a random point in the
			// width and lenght range
			int xpoint = rand() % width -1;
			int zpoint = rand() % length -1;

			// Assign random circle size
			float circleSize = rand()%20 + 10;

			// For every element inside the heightmap matrix
			for (int x = 0; x < width; x++){
				for (int z = 0; z < length; z++){

					// Calculate distance from matrix point to random point
					float distanceFromCenter = sqrt(pow(xpoint - x,2) + pow(zpoint - z,2));
					float pointDistance = distanceFromCenter * 2.0f / circleSize;

					// Apply displacement
					if (fabs(pointDistance) <= 1.0f) 
						heightmap[z][x] += displacement/2.0f + cos(pointDistance  * 3.1415f) * displacement/2.0f;
				}
			}
		}
	}

	// Update normals and heightmap colour/height range
	calculateNormals();
	findHeightestAndLowest();
}

/* Method for calculating / updating terrain
face or vertex normal vectors */
void Terrain::calculateNormals(){

	// Initialise map normals matrix
	map_normals = new Vector3f*[length];
	for (int i = 0; i < length; ++i)
		map_normals[i] = new Vector3f[width];

	// For every vertex in the heightmap matrix
	for (int z = 0; z < length; z++){
		for (int x = 0; x < width; x++){

			// Declare a default normal vector
			// and default surrounding vectorsß
			Vector3f normal(0.0f, 0.0f, 0.0f);
			Vector3f up, down, right, left;
			up = down = right = left = Vector3f(0.0f, 0.0f, 0.0f);

			// If allowed to go down
			if ( z < length - 1){
				down = Vector3f(0.0f, heightmap[z +1][x] - heightmap[z][x], 1.0f);
			}

			// If allowed to go up
			if ( z > 0){
				up = Vector3f(0.0f, heightmap[z -1][x] - heightmap[z][x], -1.0f);
			}

			// If allowed to go right
			if ( x < width - 1){
				right = Vector3f(1.0f, heightmap[z][x +1] - heightmap[z][x], 0.0f);
			}

			// If allowed to go left
			if ( x > 0){
				left = Vector3f(-1.0f, heightmap[z][x -1] - heightmap[z][x], 0.0f);
			}

			// Normal is the sum of all surrounding normal
			// calculations (the cross products)
			normal = normal + (up * left).normalized();
			normal = normal + (right * up).normalized();
			normal = normal + (left * down).normalized();
			normal = normal + (down * right).normalized();

			// Assign normal to map normal matrix
            map_normals[z][x] = normal;
		}
	}
}

/* Find heighest and lowest point
in the terrain in order to calculate
the colour interpolation */
void Terrain::findHeightestAndLowest(){

	// Assign lowest and height
	// arbitrary values
	float l = 10000.0f;
	float h = -10000.0;

	// For every vertex in the map
	for (int z = 0; z < length; ++z){
		for (int x = 0; x < width; ++x){

			// Find heighest and lowest
			if (heightmap[z][x] < l) l = heightmap[z][x];
			if (heightmap[z][x] > h) h = heightmap[z][x];

		}
	}

	// Assign final results
	heighest = h;
	lowest = l;
}

/* Convert value to 0...1 range */
float Terrain::convertToRange(float value){
	float oldr = (heighest - lowest);
	float range = 1; 
	return (((value - lowest) * range) / oldr);
}

/* Get colour interpolation given
x and z height value */
Vector3f Terrain::getVertexColour(int x, int z){
	float height = heightmap[z][x];
	height = convertToRange(height);
	return Vector3f(height, 1.0f - height, 0.0f);
}

