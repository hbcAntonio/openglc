#include "asg03.h"
#include <iostream>
using namespace::std;

/* Constructor for Assignment 03 class */
Asg03::Asg03(float msupdate){
	// Fetch terrain width and lenght
	fetchData();

	// Initialises main attributes
	this->msupdate = msupdate;
	this->camera = new AdvancedCamera(Vector3f(-20.0f, 20.0f, -20.0f));
	this->camera->rotateY(-135);
	this->camera->rotateX(20);
	this->terrain = new Terrain(twidth, tlenght);
	this->appDrawMode = SMOOTH;
}

/* Assignment 03 destructor class */
Asg03::~Asg03(){
	delete[] terrain;
	delete[] camera;
}

/* Nothing is really performed here in Assigmnent 03*/
void Asg03::update(){

}

/* Assignment 03 main display handler */
void Asg03::display(){

	// Load identity matrix
	glLoadIdentity();
	// Position camera
	gluLookAt( camera->position.x, camera->position.y, camera->position.z,
			   camera->lookat.x, camera->lookat.y, camera->lookat.z,
			   camera->up.x, camera->up.y, camera->up.z);

	// Pushes matrix to draw terrain (not really needed)
	glPushMatrix();

		// For every vertex on the terrain
		for (int z = 0; z < terrain->length -1; ++z){

			// Draw row as a triangle strip
			glBegin(GL_TRIANGLE_STRIP);

			for (int x = 0; x < terrain->width; ++x){

				// If the shade model is GL_SMOOTH
				if ( appDrawMode == SMOOTH){

					// Get terrain normal
					Vector3f vertex_normal = terrain->map_normals[z][x];
					glNormal3f(vertex_normal.x, vertex_normal.y, vertex_normal.z);
					// Get vertex colour
					defineVertexColour(x, z, true);
					glVertex3f(x, terrain->heightmap[z][x], z);

					// Get next normal
					vertex_normal = terrain->map_normals[z +1][x];
					glNormal3f(vertex_normal.x, vertex_normal.y, vertex_normal.z);
					// Get next colour
					defineVertexColour(x, z +1, true);
					glVertex3f(x, terrain->heightmap[z +1][x], z +1);

				}
				// If the shader model is GL_FLAT
				else if ( appDrawMode == FLAT){

					// Needed to calculate normal for face
					Vector3f v1, v2;
					Vector3f k(x, terrain->heightmap[z][x], z);

					// Handles terrain dimension restraints
					if (z < terrain->length -1) v1 = Vector3f(x, terrain->heightmap[z+1][x], z+1);
					else if (x < terrain->width) v1 = Vector3f(x+1, terrain->heightmap[z][x+1], z);
					else v1 = Vector3f(0.0f, 0.0f, 0.0f);

					if (x < terrain->width -1) v2 = Vector3f(x+1, terrain->heightmap[z][x+1], z);
					else if (z < terrain->length -1) v2 = Vector3f(z+1, terrain->heightmap[z+1][x], z+1);

					// Effectively calculates face normal
					Vector3f u = v1 - k;
					Vector3f v = v2 - k;
					Vector3f normal = (u * v).normalized();

					// Applies normal
					glNormal3f(normal.x, normal.y, normal.z);

					// Get vertex colour and draw face
					defineVertexColour(x, z, true);
					glVertex3f(x, terrain->heightmap[z][x], z);
					defineVertexColour(x, z +1, true);
					glVertex3f(x, terrain->heightmap[z +1][x], z +1);
				}
				// If the draw model does not require lights 
				else if ( appDrawMode == WIREFRAME){

					// Get vertices colours and draw them
					defineVertexColour(x, z, false);
					glVertex3f(x, terrain->heightmap[z][x], z);
					defineVertexColour(x, z+1, false);
					glVertex3f(x, terrain->heightmap[z +1][x], z +1);
				}
			}
			glEnd();
		}

	glPopMatrix();
}

/* Not needed at this time */
void Asg03::special(int key, int x, int y){

}

/* Handles keyboard input */
void Asg03::keyboard(unsigned char key, int x, int y){

	// Get active modifiers to see if SHIFT is active
	int mod = glutGetModifiers();

	// Strafe/Look right
	if (key == 'D' || key == 'd'){
		if (mod == GLUT_ACTIVE_SHIFT) camera->strafe(1.0f);
		else camera->rotateY(-camera->rotspeed.y);
	} 
	// Strafe/Look left
	else if (key == 'A' || key == 'a'){
		if (mod == GLUT_ACTIVE_SHIFT) camera->strafe(-1.0f);
		else camera->rotateY(camera->rotspeed.y);
	} 
	// Move/Look up
	else if (key == 'W' || key == 'w'){
		if (mod == GLUT_ACTIVE_SHIFT) camera->move(1.0f);
		else camera->rotateX(-camera->rotspeed.x);
	} 
	// Move/Look down
	else if (key == 'S' || key == 's'){
		if (mod == GLUT_ACTIVE_SHIFT) camera->move(-1.0f);
		else camera->rotateX(camera->rotspeed.x);
	}
	// Reset terrain (current algorithm)
	else if (key == 'R' || key == 'r'){
		terrain->generateTerrainPlane();
	} 
	// Reset terrain and set algorithm to circles
	else if (key == 'C' || key == 'c'){
		terrain->genAlgorithm = terrain->CIRCLES;
		terrain->generateTerrainPlane();
	} 
	// Reset terrain and set algorithm to fault
	else if (key == 'F' || key == 'f'){
		terrain->genAlgorithm = terrain->FAULT;
		terrain->generateTerrainPlane();
	} 
	// Disable/Enable heightmap colouring
	else if (key == 'H' || key == 'h'){
		terrain->drawHeightColour = !terrain->drawHeightColour;
		if (terrain->drawHeightColour){
			float l_ambient[] = {0.8f, 0.8f, 0.8f, 0.8f};
			glLightfv(GL_LIGHT0, GL_AMBIENT, l_ambient);
		} else {
			float l_ambient[] = {0.1f, 0.1f, 0.1f, 1.0f};
			glLightfv(GL_LIGHT0, GL_AMBIENT, l_ambient);
		}
	}

	// Draw modes
	if (key == '1'){
		// GL_FLAT
		glShadeModel(GL_FLAT);
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_LIGHTING);
		appDrawMode = FLAT;
	}
	if (key == '2'){
		// GL_SMOOTH
		glShadeModel(GL_SMOOTH);
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_LIGHTING);
		appDrawMode = SMOOTH;
	}
	if (key == '3'){
		// Wireframe mode
		glShadeModel(GL_SMOOTH);
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_LIGHTING);
		appDrawMode = WIREFRAME;
	}
}

/* Get width and height data according
to constraints (min 50, max 300) */
void Asg03::fetchData(){

	cout << "Please enter terrain width: ";
	cin >> twidth;

	if (twidth >= 50 && twidth <= 300){
		cout << "Please enter terrain length: ";
		cin >> tlenght;
	}

	if (tlenght < 50 || tlenght > 300) {
		cout << "Try again... \n";
		this->fetchData();
	}
}

/* Calculate vertex colour interpolation
based on x,z height */
void Asg03::defineVertexColour(int x, int z, bool light){

	// Ignore if drawHeightColour is false
	if (!terrain->drawHeightColour){
		// Set colour and material back to default
		float m_ambient[] = {0.2f, 0.2f, 0.2f, 1.0f};
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, m_ambient);
		glColor3f(1.0f, 1.0f, 1.0f);
		return;
	}

	// Calculate interpolation effectively
	Vector3f colour = terrain->getVertexColour(x,z);
	float vcolour[] = {colour.x, colour.y, colour.z, 1.0f};

	// If scene is lit, assign colour to material, else define colour for vertex
	if (light) glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, vcolour);
	else glColor4fv(vcolour);
}