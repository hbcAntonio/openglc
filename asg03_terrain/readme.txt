*** Application NEEDS console window
to read terrain width and height prior
to rendering the terrain ***

# Program extra features implemented:
- Improved camera
- Heightmap colouring
- Circles algorithm

# Program keyboard commands:

- '1' puts application in FLAT mode
- '2' puts application in SMOOTH mode
- '3' puts application in WIREFRAME mode

- WASD moves camera if SHIFT is pressed
- WASD rotates view if SHIFT is not pressed

- 'C' changes the generation algorithm to CIRCLES
- 'F' changes the generation algorithm to FAULT
- 'R' resets terrain with defined algorithm
- 'H' toggles heightmap colouring on/off
- SPACE toggles particle spawn on/off
- ESC exits the programs

* not case-sensitive

PLATFORM USED TO PROGRAM:
MacOSX
NO IDE USED:
- Makefile included