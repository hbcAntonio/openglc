#include "autility.h"

/* Autility.AColour definitions */
AUtility::AColour::AColour(float r, float g, float b){
	this->r = r;
	this->g = g;
	this->b = b;
}

/* AUtility.AVertex definitions */
AUtility::AVertex::AVertex(float x, float y){
	this->x = x;
	this->y = y;
	this->z = 0.0f;
	this->vColour = new AUtility::AColour(1.0f, 1.0f, 1.0f);
}

AUtility::AVertex::AVertex(float x, float y, AUtility::AColour *vColour){
	this->x = x;
	this->y = y;
	this->z = 0.0f;
	this->vColour = vColour;
}

AUtility::AVertex::AVertex(float x, float y, float z){
	this->x = x;
	this->y = y;
	this->z = z;

	this->vColour = new AUtility::AColour(1.0f, 1.0f, 1.0f);
}

AUtility::AVertex::AVertex(float x, float y, float z, AUtility::AColour *vColour){
	this->x = x;
	this->y = y;
	this->z = z;
	this->vColour = vColour;
}

/* Vector addition, subtraction, 
dot product, magnitude and normalization
calculations are defined below */
AUtility::AVertex* AUtility::AVertex::add(AUtility::AVertex *aVertex){
	return (new AUtility::AVertex(this->x + aVertex->x, this->y + aVertex->y, this->z + aVertex->z));
}

AUtility::AVertex* AUtility::AVertex::sub(AUtility::AVertex *aVertex){
	return (new AUtility::AVertex(this->x - aVertex->x, this->y - aVertex->y, this->z + aVertex->z));
}

float AUtility::AVertex::dot(AUtility::AVertex *aVertex){
	return (this->x * aVertex->x) + (this->y * aVertex->y) + (this->z * aVertex->z);
}

float AUtility::AVertex::mag(){
	return sqrt((this->x * this->x) + (this->y * this->y) + (this->z * this->z));
}

void AUtility::AVertex::normalize(){

	float mag = this->mag();
	this->x /= mag;
	this->y /= mag;
	this->z /= mag;
}

void AUtility::AVertex::setColour(AUtility::AColour* vColour){
	this->vColour = vColour;
}

/* AUtility.APolygon definitions */
AUtility::APolygon::APolygon(){}

/* AUtility definitions */
AUtility::AUtility(){}
