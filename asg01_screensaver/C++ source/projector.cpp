#include "projector.h"

/* Projector constructor and destructor implementations */
Projector::Projector(){

	// Starts by recognizing a click to add a true vertex
	this->state = ADD_TRUE_VERTEX;

	// Adds something to the pVertices vector
	std::vector<ProjectorVertex*> tempVector;
	this->pVertices.push_back(tempVector);

	// Random seed initialization
	srand(time(0));
}
Projector::~Projector(){}

/* ProjectorVertex constructor and destructor implementations */
Projector::ProjectorVertex::ProjectorVertex(int x, int y, AUtility::AColour* aColour){

	// Initialises new vertex with received X and Y mouse coordinates 
	aVertex = new AUtility::AVertex((float)x, (float)y);
	aVertex->setColour(aColour);

	// When initialised, vertex must not move until
	// a direction is calculated
	this->xSpeed = 0.0f;
	this->ySpeed = 0.0f;
}
Projector::ProjectorVertex::~ProjectorVertex(){}

/* ProjectorVertex getter/setter methods */
void Projector::ProjectorVertex::setXSpeed(float speed){
	this->xSpeed = speed;
}

void Projector::ProjectorVertex::setYSpeed(float speed){
	this->ySpeed = speed;
}

void Projector::ProjectorVertex::setAVertex(AUtility::AVertex *aVertex){
	this->aVertex = aVertex;
}

float Projector::ProjectorVertex::getXSpeed(void){
	return this->xSpeed;
}

float Projector::ProjectorVertex::getYSpeed(void){
	return this->ySpeed;
}

AUtility::AVertex* Projector::ProjectorVertex::getAVertex(void){
	return this->aVertex;
}

/* Projector method to add a vertex to the current polygon
   Receives mouse X and Y coordinates */
void Projector::addProjectorVertex(int x, int y, AUtility::AColour* aColour){
	
	// Adding a new vertex only happens in this 
	// state (first-click of the cycle)
	if (state == ADD_TRUE_VERTEX){
		this->pVertices[pVertices.size() -1].push_back(new ProjectorVertex(x, y, aColour));

		// Change state to proper proccess next click
		this->state = ADD_DIR_VERTEX;
	}
	// Adding a direction vertex
	else if (state == ADD_DIR_VERTEX){
		// For clarity's sake, initialises a temporary
		// pointer to the last vertex of the last polygon
		if (this->pVertices[pVertices.size()-1].size() == 0){
			this->state = ADD_TRUE_VERTEX;
			return;
		}

		ProjectorVertex *tempVertex = this->pVertices[pVertices.size()-1][pVertices[pVertices.size()-1].size()-1];
		
		// Calculates effectively the direction the
		// current vertex is going to follow
		AUtility::AVertex *mouseVertex = new AUtility::AVertex((float)x+0.01, (float)y+0.01);
		AUtility::AVertex *posVertex = new AUtility::AVertex(tempVertex->getAVertex()->x, tempVertex->getAVertex()->y);
		AUtility::AVertex *speedVertex = mouseVertex->sub(posVertex);
		speedVertex->normalize();

		// After calculated, updates the speed
		// variables so main application knows
		// what to increase/decrease when drawing
		tempVertex->setXSpeed(speedVertex->x);
		tempVertex->setYSpeed(speedVertex->y);
		
		// Change state back to adding a vertex
		resetState();
	}
}

/* Clears the vertices list, regardless of how many
polygons there are in the list. Then, introduces an
empty list for the first polygon in order to correctly
insert vertices in the addProjectorVertex method */
void Projector::clearVertices(){
	this->pVertices.clear();
	std::vector<ProjectorVertex*> tempVector;
	this->pVertices.push_back(tempVector);
	resetState();
}

/* Resets the projector state to admit a first-click
case, regardless of what has already been done */
void Projector::resetState(){
	this->state = ADD_TRUE_VERTEX;
}

/* Makes the calculations and applies them
to the projector vertices */
bool Projector::update(float generalSpeed, bool isAnimating, bool isBouncing, int w, int h){

	// If it is not animating and a direction needs to
	// be shown then a redisplay request is needed
	if (!isAnimating && state == ADD_DIR_VERTEX) return true; 
	else if (!isAnimating && state != ADD_DIR_VERTEX) return false;

	// Iterates through the whole vector of
	// ProjectorVertex vectors
	int pvertices_size = this->pVertices.size();
	for (int i = 0; i < pvertices_size; ++i){

		int vertices_size = this->pVertices[i].size();
		for (int j = 0; j < vertices_size; ++j){

			Projector::ProjectorVertex *tPVertex = this->pVertices[i][j];
			AUtility::AVertex *tAVertex = tPVertex->getAVertex();

			// Bouncing off screen edges
			if (isBouncing){
				if (tAVertex->x > w) tPVertex->setXSpeed(-fabs(tPVertex->getXSpeed()));
				if (tAVertex->x < 0) tPVertex->setXSpeed(fabs(tPVertex->getXSpeed()));
				if (tAVertex->y > h) tPVertex->setYSpeed(-fabs(tPVertex->getYSpeed()));
				if (tAVertex->y < 0) tPVertex->setYSpeed(fabs(tPVertex->getYSpeed()));				
			} else {
			// Loop mode
				if (tAVertex->x < 0) tAVertex->x = w;
				if (tAVertex->x > w) tAVertex->x = 0;
				if (tAVertex->y < 0) tAVertex->y = h;
				if (tAVertex->y > h) tAVertex->y = 0;
			}
			
			tAVertex->x += tPVertex->getXSpeed() * generalSpeed;
			tAVertex->y += tPVertex->getYSpeed() * generalSpeed;
		}
	}

	// Redisplay is needed otherwise (if animating)
	return true;
}

/* Draws the projector */
void Projector::draw(GLenum mode, int mouse_x, int mouse_y){
	
	// Iterates through the whole vector of
	// ProjectorVertex vectors
	int pvertices_size = this->pVertices.size();
	for (int i = 0; i < pvertices_size; ++i){
		glBegin(mode);
			int vertices_size = this->pVertices[i].size();
			for (int j = 0; j < vertices_size; ++j){

				AUtility::AVertex *aVertex = this->pVertices[i][j]->getAVertex();
				
				glColor3f(aVertex->vColour->r, aVertex->vColour->g, aVertex->vColour->b);
				glVertex2f(aVertex->x, aVertex->y);
				
			}
		glEnd();
	}

	// Draw arrow pointing towards direction
	if (state == ADD_DIR_VERTEX){

		// Saves current line width
		// in order to restore it later
		GLfloat f;
		glGetFloatv(GL_LINE_WIDTH, &f);
		glLineWidth(1.0f);

		// Draws a line to indicate
		// the direction of the movement
		glBegin(GL_LINES);
			glColor3f(1.0f, 1.0f, 1.0f);
			AUtility::AVertex *pVertex = pVertices[pVertices.size()-1][pVertices[pVertices.size()-1].size()-1]->getAVertex();
			AUtility::AVertex *mouseVertex = new AUtility::AVertex(mouse_x, mouse_y);
			glVertex2f(pVertex->x, pVertex->y);
			glVertex2f(mouse_x, mouse_y);
		glEnd();

		// Makes line width go back to what
		// it was before calling drawing
		glLineWidth(f);
	}
}

/* Splits polygon only if applicable */
void Projector::newPolygon(){

	// Application should only be able to add
	// a new polygon if all directions have
	// already been specified
	if (this->state == ADD_TRUE_VERTEX){
		std::vector<ProjectorVertex*> newPolyVector;
		pVertices.push_back(newPolyVector);
	}
}
