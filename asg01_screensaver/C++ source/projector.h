#include "autility.h"
#include <math.h>
#include <ctime>
#include <cstdlib>
#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#  include <GL/freeglut.h>
#endif

class Projector{

public:

	Projector();
	~Projector();

	class ProjectorVertex{

	private:
		float xSpeed, ySpeed;
		AUtility::AVertex *aVertex;

	public:
		ProjectorVertex(int, int, AUtility::AColour*);
		~ProjectorVertex();

		void setXSpeed(float);
		float getXSpeed();
		void setYSpeed(float);
		float getYSpeed();
		void setAVertex(AUtility::AVertex*);
		AUtility::AVertex* getAVertex();

	};

	std::vector<std::vector<ProjectorVertex*> > pVertices;

	void addProjectorVertex(int, int, AUtility::AColour*);
	void clearVertices();
	void resetState();
	void newPolygon();
	void draw(GLenum, int mouse_x, int mouse_y);
	bool update(float, bool, bool, int, int);

private:
	enum ProjectorCurrentStates {ADD_TRUE_VERTEX = 0, ADD_DIR_VERTEX};
	ProjectorCurrentStates state;
};