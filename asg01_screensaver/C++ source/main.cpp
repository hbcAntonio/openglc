/*#ifdef _WIN32
#include <windows.h>
#endif*/
#include "asg01app.h"
#include <iostream>

#define STD_WIDTH 800
#define STD_HEIGHT 600

/* Pointer to the Application, where 
every calculation is performed */
Asg01App *application;

/* Keyboard function, used to handle
key inputs and alter modes accordingly */
void keyboardHandler(unsigned char c, int x, int y){

	// Increase vertices speed by a factor of 1
	if (c == '+') application->changeSpeedBy(1.0f);
	// Decrease vertices speed by a factor of 1
	else if (c == '-') application->changeSpeedBy(-1.0f);
	// Starts a new polygon if SPACEBAR is pressed
	else if (c == 32) application->newPolygon();
}

/* Main menu function, handles main options
of the application, such as randomizing the scene,
clearing it and exiting the application*/
void main_menu(int value){

	// Accordingly to the value, call each
	// different application function
	if (value == 1) application->pause();
	else if (value == 2) application->animate();
	else if (value == 3) application->clearPolygons();
	else if (value == 4) application->configureBorders();
	else if (value == 5) {
		// Reshapes the window to exit GLUT
		// fullscreen mode (to standard w,h)
		glutPositionWindow(10, 10);
		glutReshapeWindow(STD_WIDTH, STD_HEIGHT);
		
		// Updates fullscreen information
		application->setFullScreen(false);
	}
	else if (value == 6){
		// Toggles GLUT full screen mode
		application->setFullScreen(true);
	}
	// If everything goes as planned, the
	// exit code will be always 0
	else{
		exit(value);
	}
}

/* Colour menu handles colour changes
when vertices are created */
void colour_menu(int value){
	application->changeVertexColour(value);
}

/* Mode menu changes the rendering
mode within main application */
void mode_menu(int value){
	application->changeState(value);
}

/* Line weight menu sets the
line width for the application */
void line_menu(int value){
	glLineWidth(value);
	application->requestRedisplay();
}

/* Line weight menu sets the
line width for the application */
void point_menu(int value){
	glPointSize(value);
	application->requestRedisplay();
}

/* Menu for pre-estabilished modes */
void pre_menu(int value){
	if (value == 0) application->randomize();
	if (value == 1) application->circle();
	if (value == 2) application->senoid();
}

void shade_menu(int value){
	glShadeModel((GLenum)value);
}

/* Acquires information from the GLUT menu
that is currently being displayed */
void glutMenuInfo(int flag, int x, int y){
	application->setMenu(flag);
}

/* Mouse function, used to send mouse information to the
main application class */
void mouseHandler(int button, int state, int x, int y){
	application->mouseEvent(button, state, x,  y);
}

/* Mouse passive function, used to update mouse coordinates
in application so the direction arrow can be drawn */
void mousePassive(int x, int y){
	application->updateMousePosition(x, y);
}

/* Callback for OpenGL Display function.
No calculations are performed at draw-time */
void display(){
	
	// Selects matrix to work with
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Clears the background and draws
	glClear(GL_COLOR_BUFFER_BIT);
	application->draw();

	// Swap the buffers in order to display
	glutSwapBuffers();
}

/* Every calculation is performed in the update
method, evey 33.333 ms, for consistency */
void update(int value){

	// Call update function from main application
	application->update();
	glutTimerFunc(33.3333, update, value);
}

/* Reshape callback function, handles resizing
properly and resets the viewport*/
void reshape(int w, int h){
	application->updateScreenSize(w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(0, w, 0, h);
	glViewport(0, 0, w, h);
}

/* Initialises the GLUT menu and all its options */
void createMenu(){

	// Listing all possible modes for application
	application->mode_menu_id = glutCreateMenu(mode_menu);
	glutAddMenuEntry("Dot Mode", 1);
	glutAddMenuEntry("Lines Mode", 2);
	glutAddMenuEntry("Polygons Mode", 3);
	glutAddMenuEntry("Trace Mode", 4);
	glutAddMenuEntry("Triangles Mode", 5);

	// Listing all possible colours for vertex creation
	application->colour_menu_id = glutCreateMenu(colour_menu);
	glutAddMenuEntry("Random",	-1);
	glutAddMenuEntry("Red", 	0);
	glutAddMenuEntry("Blue", 	1);
	glutAddMenuEntry("Green", 	2);
	glutAddMenuEntry("Magenta",	3);
	glutAddMenuEntry("Indigo",	4);
	glutAddMenuEntry("Orange", 	5);
	glutAddMenuEntry("Purple", 	6);
	glutAddMenuEntry("White", 	7);

	// Listing all line widths allowed
	application->line_menu_id = glutCreateMenu(line_menu);
	glutAddMenuEntry("Thinest",	 1);
	glutAddMenuEntry("Thin",	 2);
	glutAddMenuEntry("Medium",	 3);
	glutAddMenuEntry("Thick",	 4);
	glutAddMenuEntry("Thickest", 5);

	// Listing all point sizes allowed
	application->point_menu_id = glutCreateMenu(point_menu);
	glutAddMenuEntry("Smallest", 2);
	glutAddMenuEntry("Small",	 4);
	glutAddMenuEntry("Medium",	 6);
	glutAddMenuEntry("Big",	 	 12);
	glutAddMenuEntry("Biggest",  15);

	// Pre-estabilished modes
	application->prem_menu_id = glutCreateMenu(pre_menu);
	glutAddMenuEntry("Random", 0);
	glutAddMenuEntry("Circle", 1);
	glutAddMenuEntry("Senoid", 2);
	
	// ShadeModels
	application->shade_menu_id = glutCreateMenu(shade_menu);
	glutAddMenuEntry("Flat", (int)GL_FLAT);
	glutAddMenuEntry("Smooth", (int)GL_SMOOTH);

	// Listing all possible options for the main menu
	application->main_menu_id = glutCreateMenu(main_menu);
	glutAddSubMenu("Set application mode", application->mode_menu_id);
	glutAddSubMenu("Set vertex colour", application->colour_menu_id);
	glutAddMenuEntry("Pause application", 1);
	glutAddMenuEntry("Start animation", 2);
	glutAddMenuEntry("Clear polygon list", 3);
	glutAddSubMenu("Pre-estabilished scene", application->prem_menu_id);
	glutAddMenuEntry("Exit fullscreen", 5);
	glutAddSubMenu("Adjust line width", application->line_menu_id);
	glutAddSubMenu("Adjust point size", application->point_menu_id);
	glutAddMenuEntry("Disable bounce", 4);
	glutAddSubMenu("Shade Model", application->shade_menu_id);
	glutAddMenuEntry("Quit application", 0);
	
	// Menu will be displayed when right mouse button is down
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

/* Initialises main parameters, enables needed
OpenGL features and sets up the matrixes */
void init(){

	// Declares the main application
	application = new Asg01App();

	// Creates the GLUT menu
	createMenu();

	// Background colour
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, 0.0f, 0.0f);

	// Matrix mode configurations and
	// coord system adjustments
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, STD_WIDTH, 0, STD_HEIGHT);
	glViewport(0, 0, STD_WIDTH, STD_HEIGHT);
	
	// Starts application in fullscreen mode
	application->setFullScreen(true);

}

/* Program main function */
int main(int argc, char** argv){

	// Initialisation functions
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(STD_WIDTH, STD_HEIGHT);
	glutCreateWindow("Simple");

	// Timer is called every 33.333ms to try and
	// process 60 frames during a second
	glutTimerFunc(33.3333, update, 1);

	// Setting all the callback functions
	glutMouseFunc(mouseHandler);
	glutPassiveMotionFunc(mousePassive);
	glutKeyboardFunc(keyboardHandler);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMenuStatusFunc(glutMenuInfo);

	// Init configurations
	init();

	// Starts main loop
	glutMainLoop();
}