#include <vector>
#include <math.h>
class AUtility{
	
	public:

		AUtility();

		class AColour{
		public:
			float r, g, b, a;
			AColour(float, float, float);
			AColour(float, float, float, float);
		};

		class AVertex{
		public:
			float x, y, z;
			AColour* vColour;

			AVertex(float, float);
			AVertex(float, float, float);
			AVertex(float, float, AColour*);
			AVertex(float, float, float, AColour*);

			~AVertex();

			float mag();
			void normalize();
			AVertex* add(AVertex*);
			AVertex* sub(AVertex*);
			float dot(AUtility::AVertex *aVertex);

			void clearColour();
			void setColour(AColour*);
		};

		class APolygon{
		public:
			std::vector<AVertex*> vertices;

			APolygon();
			~APolygon();

		};

		~AUtility();
};
