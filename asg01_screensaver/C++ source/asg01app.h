#include <stdio.h>
#include "projector.h"

class Asg01App{

private:
	enum ApplicationStates {APP_DOT=1, APP_LINE, APP_POLY, APP_LINELOOP, APP_TRIANGLE};
	
	AUtility *aUtility;
	ApplicationStates appState;
	Projector *projector;
	std::vector<AUtility::APolygon*> polygons;
	std::vector<AUtility::AColour*> colours;
	int colourIndex;

	bool isOnMenu;
	bool postRedisplay;
	bool isAnimating;
	bool isPaused;
	bool isFullScreen;
	bool isBouncing;

	int dotSize;
	int lineWeight;
	int generalSpeed;
	int minVertexSpeed;
	int maxVertexSpeed;
	int minRandomPolygons;
	int maxRandomPolygons;
	int minRandomVertices;
	int maxRandomVertices;
	int mouse_x;
	int mouse_y;
	int screen_width;
	int screen_height;


public:
	Asg01App();
	~Asg01App();

	Projector* getProjector();

	void draw();
	void update();
	void pause();
	void animate();
	void randomize();
	void circle();
	void senoid();
	void clearPolygons();
	void changeState(int);
	void mouseEvent(int, int, int, int);
	void requestRedisplay();
	void changeSpeedBy(float);
	void newPolygon();
	void changeVertexColour(int);
	void updateMousePosition(int, int);
	void updateScreenSize(int, int);
	void setMenu(bool);
	void configureBorders();
	void setFullScreen(bool);

	int main_menu_id;
	int colour_menu_id;
	int mode_menu_id;
	int line_menu_id;
	int point_menu_id;
	int prem_menu_id;
	int shade_menu_id;
};