#include "asg01app.h"

/* Application initialisation (constructor),
loads defaults settings */
Asg01App::Asg01App(){

	// Random initialisation
	srand(time(NULL));
	// Utility instance
	this->aUtility = new AUtility();

	// Projector instance
	this->projector = new Projector();

	// Vertex colour list
	this->colours.push_back(new AUtility::AColour(1.0f, 0.0f, 0.0f)); 	// Red
	this->colours.push_back(new AUtility::AColour(0.0f, 0.0f, 1.0f)); 	// Blue
	this->colours.push_back(new AUtility::AColour(0.0f, 1.0f, 0.0f)); 	// Green
	this->colours.push_back(new AUtility::AColour(1.0f, 0.0f, 1.0f)); 	// Magenta
	this->colours.push_back(new AUtility::AColour(0.29f, 0.0f, 0.52f)); // Indigo
	this->colours.push_back(new AUtility::AColour(1.0f, 0.64f, 0.0f));	//Orange
	this->colours.push_back(new AUtility::AColour(0.50f, 0.0f, 0.50f));	// Purple
	this->colours.push_back(new AUtility::AColour(1.0f, 1.0f, 1.0f));	// White

	// Random colour at first
	this->colourIndex = -1;

	// Starts application in dot mode
	this->appState = APP_DOT;

	// Set to false initially
	this->isOnMenu = false;
	this->isPaused = false;
	this->postRedisplay = false;
	this->isAnimating = false;
	
	// Set to true initially
	this->isBouncing = true;

	// Drawing parameters
	this->dotSize = 7;
	this->lineWeight = 2;
	this->generalSpeed = 5;
	this->minVertexSpeed = 0;
	this->maxVertexSpeed = 30;

	// Random parameters
	this->minRandomPolygons = 1;
	this->maxRandomPolygons = 6;
	this->minRandomVertices = 5;
	this->maxRandomVertices = 15;

	// Set default drawing options
	glPointSize(this->dotSize);
	glLineWidth(this->lineWeight);
}
Asg01App::~Asg01App(){}

Projector* Asg01App::getProjector(){ return this->projector;}

void Asg01App::update(){

	// Do nothing if application is paused
	if (isPaused) return;

	// Calls the update method and request a redisplay if needed
	if (projector->update(generalSpeed, isAnimating, isBouncing, screen_width, screen_height)) requestRedisplay();

	// Every time a redisplay is requested calculate whatever is needed and 
	// tell OpenGL to refresh the screen. Note: want to make sure only one
	// glutPostRedisplay call is made, despite how many requests are made.
	if (postRedisplay){
		glutPostRedisplay();
		postRedisplay = false;
	}
}

/* Every drawing aspect within the application
occurs within this method */
void Asg01App::draw(){

	// Do nothing if application is paused
	if(isPaused) return;

	// GLEnum type to store which mode should
	// the application use when drawing
	GLenum mode;

	// Handles mode accordingly to the
	// currently specified app state
	if (appState == APP_DOT) mode = GL_POINTS;
	if (appState == APP_LINE) mode = GL_LINES;
	if (appState == APP_POLY) mode = GL_POLYGON;
	if (appState == APP_LINELOOP) mode = GL_LINE_LOOP;
	if (appState == APP_TRIANGLE) mode = GL_TRIANGLE_FAN;

	// Draw each polygon on the list calculated in the
	// recalculatePolygonVector method
	projector->draw(mode, mouse_x, mouse_y);
}

/* Pause method */
void Asg01App::pause(){

	// Invert pause state
	isPaused = !isPaused;
}

/* Starts animating */
void Asg01App::animate(){

	// Program not altered if it is paused
	if (isPaused) return;

	// Set animate to true in order
	// to start calculating animation
	isAnimating = !isAnimating;

	// Request a redisplay if stopped animating
	requestRedisplay();
}

/* Changes the state of the application
based upon the keyboard input */
void Asg01App::changeState(int mode){

	// Program not altered if it is paused
	if (isPaused) return;

	// ASCII code considerations (saves us a few ifs)
	appState = (ApplicationStates)(mode);

	// If state is changed while animation is not being
	// calculated, just request a redisplay
	if (!isAnimating) requestRedisplay();
	//printf("\nApplication stated changed to: %d", this->appState);
}

/* Handles mouse input accordingly to application state */
void Asg01App::mouseEvent(int button, int state, int x, int y){
	
	// Program not altered if it is paused or menu is open
	if (isPaused) return;

	// Inverts y coordinates
	y = screen_height - y;

	// Calls addProjectorVertex only if left button is pressed and
	// application is not displaying the GLUT menu
	if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON && !isOnMenu){
		if (colourIndex == -1) projector->addProjectorVertex(x, y, new AUtility::AColour((rand()%100)/100.0f, (rand()%100)/100.0f, (rand()%100)/100.0f));
		else projector->addProjectorVertex(x, y, colours[colourIndex]);

		// Request a redisplay
		requestRedisplay();
	}

}

/* Concentrates all requests in a single one. Cannot be paused*/
void Asg01App::requestRedisplay(){
	this->postRedisplay = true;
}

/* Called to clear all polygons from
scene by reseting the polygons vector */
void Asg01App::clearPolygons(){

	// Program not altered if it is paused
	if (isPaused) return;

	projector->clearVertices();
	projector->resetState();
	requestRedisplay();
}

/* Changes speed by a factor of A,
respecting minSpeed and maxSpeed
constraints defined */
void Asg01App::changeSpeedBy(float a){

	// Program not altered if it is paused or if menu is open
	if (isPaused || isOnMenu) return;

	// Check the minimun and maximun speed limits
	this->generalSpeed += a;

	if (this->generalSpeed < minVertexSpeed) this->generalSpeed = minVertexSpeed;
	else if (this->generalSpeed > maxVertexSpeed) this->generalSpeed = maxVertexSpeed;
}

/* Changes the index of the
colour vertex array */
void Asg01App::changeVertexColour(int i){

	// Program not altered if it is paused
	if (isPaused) return;

	this->colourIndex = i;
}

/* Randomly creates polygons and
vertices and places them in the scene */
void Asg01App::randomize(){
	this->clearPolygons();

	int vertices = (rand()%maxRandomVertices)+minRandomVertices;
	int polygons = (rand()%maxRandomPolygons)+minRandomPolygons;

	for (int i = 0; i < polygons; ++i){

		std::vector<Projector::ProjectorVertex*> tmpVector;

		for (int j = 0; j < vertices; ++j){
			AUtility::AColour *tempColour;

			if (colourIndex == -1) tempColour = new AUtility::AColour((rand()%100)/100.0f, (rand()%100)/100.0f, (rand()%100)/100.0f);
			else tempColour = colours[colourIndex];

			Projector::ProjectorVertex *pvertex = new Projector::ProjectorVertex(rand()%screen_width, rand()%screen_height, tempColour);
			int angle = (rand()%(int)(2.0f * M_PI));

			pvertex->setXSpeed(cos(angle));
			pvertex->setYSpeed(sin(angle));

			tmpVector.push_back(pvertex);
		}
		
		projector->pVertices.push_back(tmpVector);
	}
}

/* Creates a series of concentric circles
and expand them proportionally for a nice
visual effect! */
void Asg01App::circle(){
	this->clearPolygons();
	for (float i = 1.0f; i < 90.0f; i += 30.0f){
		std::vector<Projector::ProjectorVertex*> tmpVector;
		for (float j = 0.0f; j < (2.0f* M_PI); j += (1.0f/180.0f) * M_PI){
			AUtility::AColour *tempColour;

			if (colourIndex == -1) tempColour = new AUtility::AColour((rand()%100)/100.0f, (rand()%100)/100.0f, (rand()%100)/100.0f);
			else tempColour = colours[colourIndex];

			Projector::ProjectorVertex *pvertex = new Projector::ProjectorVertex((cos(j) * i) + (screen_width/2.0f), (sin(j) * i) + (screen_height/2.0f), tempColour);
			pvertex->setXSpeed(cos(j));
			pvertex->setYSpeed(sin(j));

			tmpVector.push_back(pvertex);
		}
		projector->pVertices.push_back(tmpVector);
	}
}

/* Creates a series of */
void Asg01App::senoid(){
	this->clearPolygons();
	for (int i = 0; i < 1; i ++){
		std::vector<Projector::ProjectorVertex*> tmpVector;
		for (float x = 1; x < screen_width; x += 4.5f){
			AUtility::AColour *tempColour;

			if (colourIndex == -1) tempColour = new AUtility::AColour((rand()%100)/100.0f, (rand()%100)/100.0f, (rand()%100)/100.0f);
			else tempColour = colours[colourIndex];

			Projector::ProjectorVertex *pvertex = new Projector::ProjectorVertex(x, (sin(x) * 30.0f)+ (screen_height/2.0f), tempColour);
			pvertex->setXSpeed(cos(x));
			pvertex->setYSpeed(cos(x));

			tmpVector.push_back(pvertex);
		}
		projector->pVertices.push_back(tmpVector);
	}
}
/* Creates a new polygon if all previous
are closed and with directions defined */
void Asg01App::newPolygon(){
	if(isOnMenu || isPaused) return;
	this->projector->newPolygon();
}

/* When mouse is moved, update its position
so calculations can be performed */
void Asg01App::updateMousePosition(int x, int y){
	this->mouse_x = x;
	this->mouse_y = screen_height - y;
}

/* When the screen is resized, update
the dimensions information so calculations
can be performed */
void Asg01App::updateScreenSize(int w, int h){
	this->screen_width = w;
	this->screen_height = h;
}

/* Enables/disables border bouncing */
void Asg01App::configureBorders(){
	this->isBouncing = !this->isBouncing;
}

/* Handles turning fullscreen on/off
for the current application */
void Asg01App::setFullScreen(bool flag){
	this->isFullScreen = flag;
	if (flag) glutFullScreen();
}

/* Informs application the current
menu status, in order to prevent changes 
while it's being displayed*/
void Asg01App::setMenu(bool flag){

	// Updates control variable and
	// exits if user is on the menu
	this->isOnMenu = flag;
	if (flag) return;
	
	// Updates information on the main menu
	glutSetMenu(main_menu_id);
	
	// Regarding animation status
	if (isAnimating) glutChangeToMenuEntry(4, "Stop animation", 2);
	else glutChangeToMenuEntry(4, "Play animation", 2);
	
	// Regarding fullscreen status
	if (isFullScreen) glutChangeToMenuEntry(7, "Exit fullscreen", 5);
	else glutChangeToMenuEntry(7, "Toggle fullscreen", 6);
	
	// Regarding pause status
	if (isPaused) glutChangeToMenuEntry(3, "Resume application", 1);
	else glutChangeToMenuEntry(3, "Pause application", 1);
	
	// Regarding bouncing
	if (isBouncing) glutChangeToMenuEntry(10, "Disable bounce", 4);
	else glutChangeToMenuEntry(10, "Enable bounce", 4);
}
